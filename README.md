Assignment Question are as follows:

1. Write a program to check if a date is greater than current date or not. Display proper messages after comparison.

2. Write a program to check if a date lies in a date range.

Example -
$range_date1= "2020-02-02";
$range_date2= "2020-02-08";

$date1= "2020-02-04";

Output - Selected date exists in date range.

$date1= "2020-02-09";

Output - Selected date does not exists in date range.

3. Create a form with all types of input field (text,password,textarea,radiobuttons,checkboxes) and print their input.

4. Create a CRUD operation form with fields - name, email, age, gender, phone number, bio. You are required to List all users, Add new, Update and delete users in one page.

5. Create a CRUD operation form with fields - student_name, email, rollno, gender, school_name, class. You are required to List all users, Add new, Update and delete users in different page linked together.

Page 1 > Listing of students. Click Add New User link, Edit link, delete link.
Page 2 > Add new student. Link to go back on listing page(page 1)
Page 3 > Edit existing student. Displays info of a student and updates it. Link to go back on listing page(page 1)

6. Create an operation for searching of an email or name and list all  matching records. (SQL suggestion: You LIKE in sql query)

7.  Create a working module of an admin of many sweets shop.

Module 1 > CRUD operation for shops with fields (shop_name,address,opening_time( DB column type- time. Format h:i:s), closing_time, owner_name)

Admin will have the option to add sweets for particular shop.

Module 2 > CRUD operation for sweets of a particular shop with fields(sweet_name, price).

In Module 1 : The listing of each shop will have all the sweets added for that shop. [See attached image for reference]

